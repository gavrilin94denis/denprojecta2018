package ru.mai.dep810.webapp.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Persistent;

import java.io.Serializable;

@Persistent
public class Room implements Serializable {

    private static long serialVersionUID = 1L;

    @Id
    private String id;

    private String address;
    private String description;

    private String atributes;

    public Room() {
    }

    public Room(String id, String address, String description, String atributes) {
        this.id = id;
        this.address = address;
        this.description = description;
        this.atributes = atributes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAtributes() {
        return atributes;
    }

    public void setAtributes(String atributes) {
        this.atributes = atributes;
    }
}