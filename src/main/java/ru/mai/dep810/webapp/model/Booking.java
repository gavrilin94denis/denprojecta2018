package ru.mai.dep810.webapp.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Persistent;

import java.io.Serializable;
import java.util.Date;

@Persistent
public class Booking implements Serializable {

    private static long serialVersionUID = 1L;

    @Id
    private String idRoom;

    private String idUser;
    private Date DateOfBooking;

    private String Status;

    public Booking() {
    }

    public Booking(String idRoom, String idUser, Date DateOfBooking, String Status) {
        this.idRoom = idRoom;
        this.idUser = idUser;
        this.DateOfBooking = DateOfBooking;
        this.Status = Status;
    }

    public String getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(String id) {
        this.idRoom = id;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String user) {
        this.idUser = user;
    }

    public Date getDateOfBooking() {
        return DateOfBooking;
    }

    public void setDateOfBooking(Date bookdate) {
        this.DateOfBooking = bookdate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String stat) {
        this.Status = stat;
    }
}
